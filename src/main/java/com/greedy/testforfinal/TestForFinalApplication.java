package com.greedy.testforfinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestForFinalApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestForFinalApplication.class, args);
    }

}
